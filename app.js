const express = require("express");
const app = express();
const fullName = require("./name");

app.get("/", (req, res) => {
  const { firstName, lastName } = req.query;
  res.send(`Hello ${fullName(firstName, lastName)}`);
});

app.listen(3000, () => {
  console.log("Running on port 3000");
});
