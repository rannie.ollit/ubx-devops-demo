const fullName = require("../name");

test("Returns Louis Concepcion", () => {
  expect(fullName("Louis", "Concepcion")).toBe("Louis Concepcion");
});

test("Returns world if firstName and lastName is missing", () => {
  expect(fullName(undefined, undefined)).toBe("world");
});
